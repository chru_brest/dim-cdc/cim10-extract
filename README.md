# CIM10-extract

We are giving a try to generate an easy to reuse word2cim10 table.

Our input is [the canadian cim10 list document](https://www.cihi.ca/fr).

Output is, hopefully, a CSV that links an expression to the (many) appropriate [cim10](https://en.wikipedia.org/wiki/ICD-10).


## Installation

    pip install cim10ext


## Principle

- start from the [textualized](data/Makefile) canadian cim10 list document, and **sanitize** it.
- perform the **extraction** of associations between expressions and cim10.
- then **search** for expressions in given texts

## Usage example

See [Makefile](Makefile) or [main](cim10ext/__main__.py).



## Sanitization method
Starting from the textualized input file, obtain a ready-to-parse text file.

This is implemented in [sanitization module](cim10ext/sanitization.py).

Input file is [cim10.txt](data/cim10.txt), output file is [cim10.sanitized.txt](data/cim10.sanitized.txt).


## Extraction methods
Methods used to extract CIM10 associations from the sanitized input

### Basile-Caroline

Implemented in [extractions/bascar module](cim10ext/extractions/bascar.py).

Input file is [cim10.sanitized.txt](data/cim10.sanitized.txt), output file is [assocs.csv](data/assocs.csv).


## Search methods
Methods used to, given a text, find potentially associated CIM10.

### Simple match
If all words of an expression matches a part of the input file, then add the associated CIM10.

Implemented in [searches/simple_match module](cim10ext/searches/simple_match.py).

Input is [assocs.csv](data/assocs.csv) and a textfile containing a medical text, and output is a list of CIM10 that the search method found in given text.
