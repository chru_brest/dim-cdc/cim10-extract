"""This script will operate some input file-wide modifications"""
import re


FILE = 'data/cim10.txt'
OUTFILE = 'data/cim10.post.txt'
REPLACEMENTS = (
    (r'[Hh]elicobacter [Pp]ylori \[H\.\s*[Pp]ylori\]', 'H(elicobacter) pylori'),
    # (r'([^\(]+) \(([^\)]+) de\)', '(\2 de) \1'),
    (r'\(([^\[]+) \[([A-Z]+)\]\)', '(\1) [\2]'),
    (r'\[[^\]]+[, ]+[^\]]+\]', ''),
    (r'T, incisives en', 'incisives en T'),
    (r'\s+[A-Z]\s', ' '),  # replace single letter words
    (r'\s[A-Z][a-z]\s', ''),  # replace double letter words
    # TODO: supprimer tous les mots de 3 caractères ou moins
)

# print(re.search(, '– – chronique (BCR/ABL positif [LMC])'))

# print( re.match(r'[Hh]elicobacter [Pp]ylori \[H\.\s*[Pp]ylori\]', 'Helicobacter pylori [H.Pylori]') )
# assert False

TO_REMOVE = ' \(syndrome\)', "\(syndrome d'\)", "\(phénomène d'\)", 'abdomen, ', r' \(dû à\)', r'Note: .*', r'(coder avec le cinquième caractère pour préciser le trimestre)'


def sanitized(data: str, *, log=False) -> str:
    if log:
        print(f"Applying {len(TO_REMOVE)} removals…")
    for reg in TO_REMOVE:
        data = re.sub(reg, '', data)

    for idx, (sub, repl) in enumerate(REPLACEMENTS, start=1):
        if log:
            print(f"\rApplying {idx}/{len(REPLACEMENTS)} replacements…", end='', flush=True)
        data = re.sub(sub, repl, data)
    return data


if __name__ == '__main__':
    print(f"Reading {FILE} to populate {OUTFILE}")
    with open(FILE) as fd:
        data = fd.read()

    data = sanitized(data, log=True)


    print(f"Writing to {OUTFILE}…")
    with open(OUTFILE, 'w') as fd:
        fd.write(data)
