"""Routines to post treat the output csv data"""

import csv
import argparse
from collections import defaultdict

import model


def parse_cli() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('csvfile', type=str, help='the CSV file containing the output data')
    parser.add_argument('outfile', type=str, help='where to write amended/posttreated data')
    parser.add_argument('--minor', action='store_true', help='whether the modification is minor or not', default=False)
    return parser.parse_args()

if __name__ == '__main__':
    args = parse_cli()

    print(f"Loading the model…")
    m = model.CIM10(args.outfile)
    print(m)
    # print(f"Computing the seealso backrefs…")
    # m.enrich_cim10_with_seealso()

    # m.save_to(args.outfile)


    with open('../vision/examples/data/processed_document/one.md') as fd:
        d = fd.read()
    # print(d)
    foundcim10 = m.all_for_text(d)
    print('CIM10s:', len(foundcim10), ', '.join(foundcim10))
