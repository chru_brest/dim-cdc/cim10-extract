from poc import sanitize_and_parse_line as parseline
from poc import parsecim10, parse_text_with_paren_as_option, expandlines


def test_expansion():
    data = [
        '– moelle épinière (staphylococcique) (toute localisation) (toute partie) G06.1',
        '– – tuberculeux A17.8† G07*'
    ]
    assert set(expandlines(data)) == {
        ('moelle épinière', '', '', '', 'G06.1', ''),
        ('moelle épinière|staphylococcique', '', '', '', 'G06.1', ''),
        ('moelle épinière|toute localisation', '', '', '', 'G06.1', ''),
        ('moelle épinière|toute partie', '', '', '', 'G06.1', ''),
        ('moelle épinière|tuberculeux', '', 'A17.8', 'G07', '', ''),
        ('moelle épinière|staphylococcique|tuberculeux', '', 'A17.8', 'G07', '', ''),
        ('moelle épinière|toute localisation|tuberculeux', '', 'A17.8', 'G07', '', ''),
        ('moelle épinière|toute partie|tuberculeux', '', 'A17.8', 'G07', '', ''),
    }



def test_seealso_expension():
    data = [
        'A A00.0',
        '– B (voir aussi C, D, E)',
        'C C00.0',
        '- D D00.0',
        '- - E E00.0',
    ]
    assert set(expandlines(data)) == {
        ('A', '', '', '', 'A00.0', ''),
        ('C', '', '', '', 'C00.0', ''),
        ('C|D', '', '', '', 'D00.0', ''),
        ('C|D|E', '', '', '', 'E00.0', ''),
        ('A|B', '', '', '', '', 'E00.0'),
    }


def test_parse_text_with_paren_as_option():
    assert parse_text_with_paren_as_option('Abaissement') == ['Abaissement']
    assert parse_text_with_paren_as_option("– – plaie (en guérison), site de chirurgie récente") == ['– – plaie', ['en guérison']]
    assert parse_text_with_paren_as_option('hypertensif (voir Maladie, coeur, hypertensive)') == ['hypertensif', ['voir Maladie, coeur, hypertensive']]

def test_parsecim10():
    assert parsecim10("Abaissement") == ('Abaissement', '', '', '')
    assert parsecim10("praecox I89.0") == ( 'praecox', '', '', 'I89.0')
    assert parsecim10(" – conjonctivite B60.1† H13.1*") == (
        '– conjonctivite', 'B60.1', 'H13.1', ''
    )
    assert parsecim10("– herpès (simplex) (virus) B00.5 H13.1*") == (
        '– herpès (simplex) (virus)', '', 'H13.1', 'B00.5'
    )

def test_parseline_basics():
    assert parseline("Abaissement") == (
        0, ['Abaissement'], '', '', '', '', []
    )

    assert parseline("praecox I89.0") == (
        0, ['praecox'], '', '', '', 'I89.0', []
    )

    assert parseline("– – hypertensif (voir Maladie, coeur, hypertensive)") == (
        2, ['hypertensif'], '', '', '', '', ['Maladie', 'coeur', 'hypertensive']
    )

    assert parseline("– abdomen, abdominal") == (
        1, ['abdominal'], '', '', '', '', []
    )


def test_parseline_options():
    assert parseline("– taux d'hémoglobine (voir aussi Anémie) D64.9") == (
        1, ["taux d'hémoglobine"], '', '', '', 'D64.9', ['Anémie']
    )
    assert parseline("– - Abcès (embolique) (infectieux) (métastatique) (multiple) (pyogène) (septique) (avec) (de) L02.9") == (
        2, ['Abcès', ['embolique', 'infectieux', 'métastatique', 'multiple', 'pyogène', 'septique', 'avec', 'de']], '', '', '', 'L02.9', []
    )
    assert parseline("– amygdale(s) J36") == (
        1, ['amygdale', ['s']], '', '', '', 'J36', []
    )
    assert parseline("– – plaie (en guérison), site de chirurgie récente") == (
        2, ['plaie', ['en guérison']], '', '', '', '', []
    )





def test_parseline_options_before_line():
    assert parseline("XXXX (syndrome), femme Q97.1") == (
        0, ['XXXX'], '', '', '', 'Q97.1', []
    )


def test_parseline_modifiers_only():
    assert parseline("– aide médicale à mourir [AMAM] Z51.81") == (
        1, ["aide médicale à mourir"], 'AMAM', '', '', 'Z51.81', []
    )

def test_parseline_options_and_modifiers():
    assert parseline("– multisystématisée (cerveau) (système nerveux central) (type parkinsonien) [AMS-P] G23.2") == (
        1, ['multisystématisée', ['cerveau', 'système nerveux central', 'type parkinsonien']], 'AMS-P', '', '', 'G23.2', []
    )

def test_parseline_double_cim():
    assert parseline(" – conjonctivite B60.1† H13.1*") == (
        1, ['conjonctivite'], '', 'B60.1', 'H13.1', '', []
    )
    assert parseline("Xérophtalmie (carence en vitamine A) E50.7 H19.8*") == (
        0, ['Xérophtalmie', ['carence en vitamine A']], '', '', 'H19.8', 'E50.7', []
    )


def test_weirdos():
    assert parseline('– – âge mûr [MODY] (dû à)') == (
        2, ['âge mûr'], 'MODY', '', '', '', []
    )

def test_cim10_paren():
    data = '– – microvasculaires intrarétiniennes [AMIR] (E10.31/H36.0*) (E11.31/H36.0*) (E13.31/H36.0*) (E14.31/H36.0*)'
    assert parsecim10(data) == ('– – microvasculaires intrarétiniennes [AMIR]', '', 'H36.0', 'E10.31|E11.31|E13.31|E14.31')
    assert parseline(data) == (
        2, ['microvasculaires intrarétiniennes'], 'AMIR', '', 'H36.0', 'E10.31|E11.31|E13.31|E14.31', []
    )
