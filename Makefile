



s: sanitize
sanitize:
	python -m cim10ext sanitize data/cim10.txt data/cim10.sanitized.txt

e: extract
extract:
	python -m cim10ext extract data/cim10.test.txt assocs.csv

f: search
search:
	python -m cim10ext search assocs.csv -i "c'est un lupus"
	python -m cim10ext search assocs.csv ../vision/examples/data/processed_document/one.md




# OLD API
r: run
run:
	python poc.py data/cim10.sanitized.txt out.csv

p: postreat
postreat:
	python posttreatments.py out.csv out.post.csv

t: test
test:
	python -m pytest tests.py --doctest-modules -vv


