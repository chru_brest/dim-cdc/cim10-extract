"""This script will operate some input file-wide modifications"""
import re

REPLACEMENTS = (
    (r'[Hh]elicobacter [Pp]ylori \[H\.\s*[Pp]ylori\]', 'H(elicobacter) pylori'),
    # (r'([^\(]+) \(([^\)]+) de\)', '(\2 de) \1'),
    (r'\(([^\[]+) \[([A-Z]+)\]\)', '(\1) [\2]'),
    (r'\[[^\]]+[, ]+[^\]]+\]', ''),
    (r'T, incisives en', 'incisives en T'),
    (r'\s+[A-Z]\s', ' '),  # replace single letter words
    (r'\s[A-Z][a-z]\s', ''),  # replace double letter words
    # TODO: supprimer tous les mots de 3 caractères ou moins
)

# print(re.search(, '– – chronique (BCR/ABL positif [LMC])'))

# print( re.match(r'[Hh]elicobacter [Pp]ylori \[H\.\s*[Pp]ylori\]', 'Helicobacter pylori [H.Pylori]') )
# assert False

TO_REMOVE = ' \(syndrome\)', "\(syndrome d'\)", "\(phénomène d'\)", 'abdomen, ', r' \(dû à\)', r'Note: .*', r'(coder avec le cinquième caractère pour préciser le trimestre)'


def sanitized(data: str, *, log=False) -> str:
    if log:
        print(f"Applying {len(TO_REMOVE)} removals…")
    for reg in TO_REMOVE:
        data = re.sub(reg, '', data)

    for idx, (sub, repl) in enumerate(REPLACEMENTS, start=1):
        if log:
            print(f"\rApplying {idx}/{len(REPLACEMENTS)} replacements…", end='', flush=True)
        data = re.sub(sub, repl, data)
    return data


def from_file(input_file: str, output_file: str, *, row_limit: int = 0):
    print(f"Reading {input_file} to populate {output_file}")
    with open(input_file) as fd:
        data = fd.read()

    data = sanitized(data, log=True)
    if row_limit and row_limit > 0:
        data = ''.join(data.splitlines(True)[:row_limit])

    print(f"Writing to {output_file}…")
    with open(output_file, 'w') as fd:
        fd.write(data)
