"""Extraction method created by Basile and Caroline.


Principle:

Nous avons bossé avec Caroline pour te donner des règles pour le parsing, les voici :

Début du parsing = page 37
Fin du parsing = page 1395
Règle de parsing CIM-10 Extract           :
- Tous niveau : Ne pas tenir compte des mots entre parenthèses sauf si toutes les lettres sont en majuscules (acronymes)
- Si pas de code CIM-10 = Supprimer
- Ne pas tenir compte des (voir aussi ...)
- Si code dague + astérisque, il faut garder les 2 codes CIM-10
- Si plusieurs codes CIM-10 tous les prendre
- Si ',' = OU
- Ne pas tenir compte de 'Siège', 'non précisé', 'NCA', 'non spécifié' si seul mot/expression dans la ligne
- Supprimer les stopword (a, et, ou, le, la, une, des, etc.)
- Ne pas prendre les tableaux de la page 1112 à 1344
-Pour les tumeurs page 1346 à 1395 : appliquer l'algorithme suivant :
Tumeur + mot(s) 1 colonne 1 + titre colonne 2 = code 1 colonne 2
Tumeur + mot(s) 7 colonne 1 + titre colonne 3 = code 7 colonne 3
etc.

Nomenclature:
Niveau 1 = le(s) mot(s) en gras
Niveau 2 = -
Niveau 3 = --
Niveau 4 = ---

Ceci sera très spécifique et peu sensible mais c'est une bonne approche


"""

from cim10ext import blocparser
from pprint import pprint

def extract_associations_from_sanitized(input_file: str, output_file: str):
    model = blocparser.parse_file(input_file)
    model = [
        bloc.as_dict(paren=False, okparen=True, nocim10=False, stopwords=False, seealso=False)
        for bloc in model.blocs
    ]
    pprint(model)

