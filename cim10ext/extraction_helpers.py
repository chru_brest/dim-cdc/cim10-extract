"""Function to help one with the extraction task"""
import re
import csv
import textx
import itertools
from collections import namedtuple

record = namedtuple('record', 'level, text, ref, cause, star, cim10, seealso')
record.__new__.__defaults__ = '', '', '', '', []


def read_lines(fname: str = 'data/cim10.txt'):
    """Read sanitized CIM10 file, yielding only relevant lines"""
    def extract_lines(reader):
        while True:
            line = next(reader)
            if not line:
                continue  # no data
            elif line.startswith(' '):
                continue  # title or letter section opening
            elif line.startswith('Section '):
                assert next(reader).startswith('traumatisme')  # pass the next line, which is also a title
                continue
            yield line
    with open(fname) as fd:
        try:
            yield from extract_lines(map(str.rstrip, fd))
        except RuntimeError:
            return


