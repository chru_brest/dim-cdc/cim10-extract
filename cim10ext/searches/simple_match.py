""""""

import cim10ext

def search_for_cim10_in_text(associations_file: str, text: str) -> list[str]:
    model = cim10ext.model.CIM10(associations_file)
    return model.all_for_text(text)

