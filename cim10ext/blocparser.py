"""Grammar for parsing the whole CIM10 file and extract all relevant entities"""

import textx
from collections import namedtuple
from cim10ext import extraction_helpers


# NOTE: – != -
GRAMMAR = """
Model: blocs*=Bloc;
Bloc: l1line=LineContent lines*=SubLine;
SubLine: levelmark=Level content=LineContent;
LineContent: elems*=Elem '\n';
Level: /((– ){1,3})/;
Elem: Cim10 | SeeAlso | Tag | OKParen | Paren | Word;
Tag: match=/(\[[^\]]+\])/;
SeeAlso: match=/(\(voir aussi[^\)]+\))/;
OKParen: match=/(\([A-Z0-9]+\))/;
Paren: match=/(\([^\)]+\))/;
Word: match=/([^\s–,]+)/ /,?/;
Cim10: match=/([A-Z][0-9]+\.[0-9]+)[†\*]?/ | match=/([A-Z][0-9]+)[†\*]?/;
"""
STOP_WORDS = 'de', 'le', "l'", 'ne', 'la'

ShowConfig = namedtuple('ShowConfig', 'paren, okparen, nocim10, stopwords, tag, seealso')
ShowConfig.__new__.__defaults__ = True, True, True, True, True


class Bloc:
    def __init__(self, parent, l1line, lines):
        self.parent = parent
        self.l1line, self.lines = l1line, lines

    def __str__(self):
        return "BLOC: {}\n {}\n".format(self.l1line, '\n '.join(map(str, self.lines)))

    def as_dict(self, **kwargs) -> dict:
        showconfig = ShowConfig(**kwargs)
        return {
            'line': self.l1line.as_dict(showconfig),
            'subs': [
                c.as_dict(showconfig, self) for c in childs_of(self)
            ]
        }


class SubLine:
    def __init__(self, parent, levelmark, content):
        self.parent = parent
        self.levelmark, self.content = levelmark, content

    def __str__(self):
        return f"{self.levelmark} {self.content}"

    def as_dict(self, showconfig: ShowConfig, bloc: Bloc) -> dict:
        return {
            'line': self.content.as_dict(showconfig),
            'subs': [
                c.as_dict(showconfig, bloc) for c in childs_of(bloc, self)
            ]
        }

    def level(self) -> int:
        assert '–' != '-'  # the first is used as level marker. The second is not.
        assert self.levelmark.count('– ') == self.levelmark.count('–')
        assert self.levelmark.count('– ') == self.levelmark.count(' ')
        assert self.levelmark.count('– ') == len(self.levelmark) // 2
        return self.levelmark.count('– ')


class LineContent:
    def __init__(self, parent, elems):
        self.parent = parent
        self.elems = elems

    def __str__(self):
        return f"{self.elems}"

    def as_dict(self, showconfig: ShowConfig) -> dict:
        cim10s = [e for e in self.elems if e.name() == 'cim10']
        others = [e for e in self.elems if e.name() != 'cim10']
        if not showconfig.stopwords:
            others = [e for e in others if e.lower() not in STOP_WORDS]
        for filterable in 'okparen,paren,tag,seealso'.split(','):
            if not getattr(showconfig, filterable):
                others = [e for e in others if e.name() != filterable]
        return {
            'elems': others,
            'cim10': cim10s,
        }


def childs_of(bloc: Bloc, parent: SubLine | type(None) = None) -> list[SubLine]:
    "List of SubLine instances that are direct child of given parent in given bloc, or direct child of the L1Line of given bloc if parent is None"
    if parent is None:  # we want the child of the very first bloc line
        yield from (l for l in bloc.lines if l.level() == 1)
    else:
        assert isinstance(parent, SubLine), parent
        target_level = parent.level() + 1
        is_yielding = False
        for line in bloc.lines:
            if line is parent:
                is_yielding = True
            if is_yielding:
                if line.level() < target_level:  # go back to grand parent
                    break
                elif line.level() > target_level:
                    continue  # exhaust grand children
                else:  # we got a child !
                    yield line

# auto-generation of the numerous Elem types
def elem_metaclass(name, bases, attrs) -> type:
    def create_funcs(name) -> tuple[callable, callable]:
        def init(self, parent, match):
            self.parent = parent
            self.match = match
        def __str__(self):
            return f'{name}({self.match})'
        def __repr__(self):
            return str(self)
        def as_dict(self, showconfig: ShowConfig) -> dict:
            return {'type': name, 'value': self.match}
        return {
            '__init__': init,
            '__str__': __str__,
            '__repr__': __repr__,
            'lower': lambda s: s.match.strip('()').lower(),
            'name': lambda _: name.lower(),
            'as_dict': as_dict,
        }
    basefuncs = create_funcs(name)
    basefuncs.update(attrs)
    return type(name, bases, basefuncs)

all_elems_classes = [
    elem_metaclass(elem, (), {})
    for elem in 'Tag,Cim10,SeeAlso,OKParen,Paren,Word'.split(',')
]

METAMODEL = textx.metamodel_from_str(GRAMMAR, classes=[Bloc, SubLine, LineContent, *all_elems_classes], skipws=True, ws=' \t')  # keep newlines


def parse_file(input_file: str) -> textx.model:
    """Return a textx model, ready to be used by extractors"""
    content = '\n'.join(extraction_helpers.read_lines(input_file)) + '\n'  # keep only interesting lines + trailing newline for ending the last Bloc
    model = METAMODEL.model_from_str(content)
    return model
