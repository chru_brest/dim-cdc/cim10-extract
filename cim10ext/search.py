"""List of all search methods"""

import cim10ext

methods = {
    'simple_match': cim10ext.searches.simple_match,
}
