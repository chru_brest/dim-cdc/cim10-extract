
import re
import csv
from collections import defaultdict


class CIM10:

    def __init__(self, input_csv_file: str):
        with open(input_csv_file) as fd:
            self.csv_data = tuple(csv.reader(fd))
        self.__load()

    def __load(self):
        self.data = defaultdict(dict)
        self.patterns = {}
        for text, ref, cause, star, cim10, seealso in self.csv_data:
            head = text.split('|', 1)[0]
            all_cim10s = cause.split('|') + star.split('|') + cim10.split('|')# + seealso.split('|')
            self.data[head.lower()][raw_pattern_for(text.lower())] = set(c for c in all_cim10s if c)

    def all_for_text(self, text: str) -> list[str]:
        "Return sorted and uniq list of cim10s that may be associed with given text"
        return sorted(list(set(self.gen_all_for_text(text))))

    def gen_all_for_text(self, text: str) -> list[str]:
        "Generate all cim10s that may be associed with given text"
        text = text.lower()
        candidates = ((head, sub) for head, sub in self.data.items() if head in text)
        for head, sub in candidates:
            best_cim10s, best_score = [], 0
            # print(f"Found {head=}")
            for pattern, cim10s in sub.items():
                score = len(pattern)
                if score > best_score and re.search(pattern, text):
                    # print(f"Found better: {score=} ({best_score=}) {cim10s=}")
                    best_score = score
                    best_cim10s = cim10s
            if best_cim10s:
                yield from best_cim10s

    def enrich_cim10_with_seealso(self):
        "Read seealso column, and tries to find associated cim10, which, if any, will be appended to cim10 column"
        wait_for_backlink = {  # line index -> seealso payload
            idx: tuple(line[-1].split('|'))
            for idx, line in enumerate(self.csv_data)
            if line[-1]  # it has a seealso
        }
        # for each different seealso, find in which lines its pattern appears
        backlinks = defaultdict(set)  # seealso -> gotten cim10
        for seealso in set(wait_for_backlink.values()):
            # print(f"For {seealso=}:")
            for cim10 in self.best_for_text(' '.join(seealso)):
                # print(f"\tfound CIM10 {cim10=}")
                backlinks[seealso].add(cim10)
                if cim10.startswith('I38'):
                    print(f"Found CIM10 {cim10=} for {seealso=}")
        print(f"{len(backlinks)=} {backlinks=}")

        # for each CSV line, add cim10 associated to its seealso
        for idx, seealso in wait_for_backlink.items():
            supp_cim10 = backlinks.get(seealso, ())
            if supp_cim10:
                self.csv_data[idx][-2] += '|'.join(supp_cim10)

        # recreate internal data from the CSV data
        self.__load()


    def save_to(self, outfile:str, *, format='csv'):
        if format == 'csv':
            with open(outfile, 'w') as fd:
                writer = csv.writer(fd)
                for line in self.csv_data:
                    writer.writerow(line)

    def lines(self, type=list):
        "Iterator over all lines"
        if type is list:
            for text, ref, cause, star, cim10, seealso in self.data.values():
                yield text, ref, cause, star, cim10, seealso
        else:
            for text, ref, cause, star, cim10, seealso in self.data.values():
                yield text, ref, '|'.join(cause), '|'.join(star), '|'.join(cim10), '|'.join(seealso)


def raw_pattern_for(splittable_text: str) -> str:
    return r'\s*'.join(sa.lower() for sa in splittable_text.replace("(", r"\(").replace(")", r"\)").split('|'))
