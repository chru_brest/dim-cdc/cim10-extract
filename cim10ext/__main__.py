""""""

import sys
import argparse

import cim10ext


def parse_cli() -> argparse.Namespace:
    main_parser = argparse.ArgumentParser(description=__doc__)
    subs = main_parser.add_subparsers(dest='recipe')

    # sanitization recipe
    parser = subs.add_parser('sanitize', description="extraction and sanitization from the input pdf file")
    parser.add_argument('input_file', type=str, help="text file containing the sanitized cim10 document")
    parser.add_argument('output_file', type=str, help="where to write the sanitized text file")
    parser.add_argument('--row-limit', '-l', type=int, help="Number of lines to keep, (for making a preview), or 0 to keep them all", default=0)

    # extraction recipe
    parser = subs.add_parser('extract', description="extraction")
    parser.add_argument('input_file', type=str, help="text file containing the sanitized cim10 document")
    parser.add_argument('output_file', type=str, help="csv file in which associations will be written")
    parser.add_argument('--method', '-m', type=str, help="", default='bascar')

    # search recipe
    parser = subs.add_parser('search', description="extraction")
    parser.add_argument('input_associations', type=str, help="csv file containing the CIM10 associations extracted from data")
    parser.add_argument('input_file', type=str, help="text file containing text to search for CIM10")
    parser.add_argument('--inline', '-i', action='store_true', help="input file is not a file, but the text itself")
    parser.add_argument('--method', '-m', type=str, help="", default='simple_match')

    return main_parser.parse_args()


if __name__ == '__main__':
    args = parse_cli()
    print('ARGS:', args)

    if args.recipe == 'sanitize':
        cim10ext.sanitization.from_file(args.input_file, args.output_file, row_limit=args.row_limit)

    elif args.recipe == 'extract':
        method = cim10ext.extraction.methods.get(args.method)
        if method:
            method.extract_associations_from_sanitized(args.input_file, args.output_file)
        else:
            print(f"Method {args.method} does not exists. Available: {', '.join(cim10ext.extraction.methods)}.")
            exit(1)

    elif args.recipe == 'search':
        method = cim10ext.search.methods.get(args.method)
        if method:
            text = args.input_file if args.inline else open(args.input_file).read()
            print(', '.join(map(str, method.search_for_cim10_in_text(args.input_associations, text))))
        else:
            print(f"Method {args.method} does not exists. Available: {', '.join(cim10ext.searches.methods)}.")
            exit(1)

    print('\nDone!')
