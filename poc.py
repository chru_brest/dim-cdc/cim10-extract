
import re
import csv
import itertools
from collections import namedtuple

import sanitize as sanitize_module

record = namedtuple('record', 'level, text, ref, cause, star, cim10, seealso')
record.__new__.__defaults__ = '', '', '', '', []


def parse_text_with_paren_as_option(text: str) -> list[str|list[str]]:
    "Will transform a text like 'a (b) (c) d' into a list like [a, [b, c], d]"
    return [e for e in _parse_text_with_paren_as_option(text) if e and e != ' ']

def _parse_text_with_paren_as_option(s: str):
    def output_string(s: str):
        if ',' in s:
            return s.split(',')[0].strip()  # drop what's after the comma ; usually its precisions we do not care about
            # return list(map(str.strip, s.split(',')))
        return s.strip()
    s = s.replace(') (', '|')
    regex_parens = re.compile(r'\(([^\(\)]+)\)')
    curr_index = 0
    for match in regex_parens.finditer(s):
        grp = match.groups()[0].split('|')
        yield output_string(s[curr_index:match.start()])
        yield grp# + ['']  # add the non-present option
        curr_index = match.end()
    if curr_index < len(s):
        yield output_string(s[curr_index:])

def read_lines(fname: str = 'data/cim10.txt'):
    def extract_lines(reader):
        while True:
            line = next(reader)
            if not line:
                continue  # no data
            elif line.startswith(' '):
                continue  # title or letter section opening
            elif line.startswith('Section I'):
                assert next(reader).startswith('traumatisme')  # pass the next line, which is also a title
                continue
            yield line
    with open(fname) as fd:
        try:
            yield from extract_lines(map(str.rstrip, fd))
        except RuntimeError:
            return

def parsevoiraussi(line: str) -> str:
    "return content of (voir aussi .*)"
    regstart = len(line)  # index at which cim10 starts
    regex = re.compile(r'\(voir\s*(aussi)? (.+)\)')
    seealso = None
    for match in regex.finditer(line):
        assert seealso is None, (line, match, seealso)
        regstart = match.start()
        seealso = delete_parenthesed(match.groups()[1])
    before = line[:regstart]
    return before, seealso

def delete_parenthesed(text: str) -> str:
    "Return the same text, minus parenthesis and their content"
    return re.sub(r'\(.*\)', '', text).strip()


def parsecim10(line: str) -> (str, str, str, list[str]):
    "extract cim10 if any"
    cim_start = len(line)  # index at which cim10 starts
    regex_cim10 = re.compile(r"([A-Z][0-9]+(\.[0-9]+)?)([\*†]?)\s*")  #
    cause, star, cim10 = [], [], []
    for match in regex_cim10.finditer(line):
        cim_start = min(cim_start, match.start())
        code, _, codetype = match.groups()
        if codetype == '†':
            cause.append(code)
        elif codetype == '*':
            star.append(code)
        else:
            cim10.append(code)
    before = line[:cim_start].strip(' (')
    canon = lambda l: sorted(list(set(l)))
    return before, '|'.join(canon(cause)), '|'.join(canon(star)), '|'.join(canon(cim10))


def sanitize_and_parse_line(line: str) -> (int, list[str|list[str]], str, str):
    return parseline(sanitize_module.sanitized(line))
def parseline(line: str) -> (int, list[str|list[str]], str, str):
    if ' ' not in line.strip():  # handle solitary words
        return record(0, [line.strip()])

    before, cause, star, cim10 = parsecim10(line)
    before, voiraussi = parsevoiraussi(before)
    voiraussi = list(map(str.strip, voiraussi.split(','))) if voiraussi else []


    # now parse the real thing
    regex = re.compile(r"(([–-] )*)?([^\[\[]*)\s*(\[.*\]\s*)?\s*")
    if match := regex.fullmatch(before):
        grp = match.groups()
        level = len(grp[0].replace(' ', ''))
        text = parse_text_with_paren_as_option(grp[2].strip())
        ref = (grp[3] or '').strip('[] ')
        return record(level, text, ref, cause, star, cim10, voiraussi)

    raise ValueError(f"A line was not matched by {regex}: {repr(before)}")


def expandlines(lines: list[str], *, sanitize: bool = True) -> list[str, str, str, str, str]:
    "Produce the output lines"

    def combinations_of(text: list[str|list[str]]):
        "Generate all possible combinations for given text"
        if not text: return
        first, *lasts = text
        if lasts:
            if isinstance(first, str):
                yield from ((first, *cs) for cs in combinations_of(lasts))
            elif isinstance(first, list):
                for sub in first:
                    yield from ((sub, *cs) for cs in combinations_of(lasts))
                    yield from combinations_of(lasts)
            else:
                assert False
        else:  # no remaining text
            if isinstance(first, str):
                yield [first]
            elif isinstance(first, list):
                for sub in first:
                    yield [sub]
                yield []  # also yield the case where the sub is missing
            else:
                assert False

    lines = map(sanitize_and_parse_line if sanitize else parseline, lines)
    context = []  # index is level, value is a parsed line
    for line in lines:
        level, text, ref, cause, star, cim10, seealso = line
        if level <= len(context):
            context = context[:level]
        context.append(line)
        fullline = list(itertools.chain.from_iterable(c.text for c in context))
        for cmb in combinations_of(fullline):
            yield '|'.join(cmb), ref, cause, star, cim10, '|'.join(seealso)



if __name__ == '__main__':
    lines = read_lines('data/cim10.post.txt')
    expandedlines = expandlines(lines, sanitize=False)  # we expect input data to be already sanitized
    with open('out.csv', 'w') as fd:
        writer = csv.writer(fd)
        for line in expandedlines:
            writer.writerow(line)
